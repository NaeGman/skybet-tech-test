Feature: As an internal trading engine consumer, I should be able to retreive a fixture

    Background:
        Given url baseUrl

        
    Scenario: When requesting the /fixture/{id} endpoint, I should see a fixture
        # configure retry's individually for the creation of a fixture's random latency time
        # after 5 failed requests, it's reasonable to assume the service may be unvailable or unreachable
        * configure retry = { count: 5, interval: 4000 }
        Given path '/fixture'
        And request read('../resources/fixtures/getFixtureById/fixture.json')
        When method POST
        # a fixture can be created if the status is 202 or 403
        * def result =  responseStatus ? responseStatus == 403 : responseStatus == 202
        * print ('Successfully Created a fixture')

        Given path '/fixture/' + 4
        # handle expected latency by adding retry until the status = 200 and the fixture is available
        And retry until responseStatus == 200
        When method GET
        Then status 200
        * match response.footballFullState.teams == '#[2]'
        * match response.footballFullState.teams[0].association == 'HOME'
        * match response.footballFullState.teams[0].name == 'Real-Madrid-CF'
        * match response.footballFullState.teams[0].teamId == 'HOME'
        * match response.footballFullState.teams[1].association == 'AWAY'
        * match response.footballFullState.teams[1].name == 'FC-Barcelona'
        * match response.footballFullState.teams[1].teamId == 'AWAY'

    
    Scenario: When requesting the /fixture{id} endpoint, I should see an updated fixture
        * configure retry = { count: 5, interval: 4000 }
        Given path '/fixture'
        And request read('../resources/fixtures/getFixtureById/fixture.json')
        When method POST
        * def result =  responseStatus ? responseStatus == 403 : responseStatus == 202
        * print ('Successfully Created a fixture')

        Given path '/fixture'
        And request read('../resources/fixtures/getFixtureById/updateFixture.json')
        When method PUT
        Then status 204

        Given path '/fixture/' + 4
        And retry until responseStatus == 200
        When method GET
        Then status 200
        * match response.footballFullState.goals[1].clockTime == 641
        * match response.footballFullState.goals[1].id == 678607
        * match response.footballFullState.goals[1].teamId == '2'

        # delete the fixture
        Given path '/fixture/' + 4
        When method DELETE
        Then status 204


    Scenario: When requesting the /fixture/{id} endpoint, using a deleted fixtureId,
    I should receive a 404 - not found HTTP Error
        * configure retry = { count: 5, interval: 4000 }
        # create and store a fixture
        Given path '/fixture'
        And request read('../resources/fixtures/getFixtureById/fixture.json')
        When method POST
        * def result =  responseStatus ? responseStatus == 403 : responseStatus == 202
        * print ('Successfully Created a fixture')

        # verify it is available
        Given path '/fixture/' + 4  
        # handle expected latency by adding retry until the status = 200 and the fixture is available
        And retry until responseStatus == 200
        When method GET
        Then status 200

        # delete the fixture
        Given path '/fixture/' + 4
        When method DELETE
        Then status 204

        # check the deleted fixture get's a 404 error
        Given path '/fixture/' + 4
        When method GET
        Then status 404
        
    Scenario: When requesting the /fixture/{id} endpoint, using a non-existing fixtureId,
    I should receive a 404 - not found HTTP error
        Given path '/fixture/11'
        When method GET
        Then status 404

