Feature: As an internal trading engine consumer, I should be able to retreive fixtures

    Background:
        Given url baseUrl

    Scenario: When requesting the /fixtures endpoint, I should see 3 fixtures and each fixture 
    should have the fixtureId field present 
        Given path '/fixtures'
        When method GET
        Then status 200
        * match response == read('../resources/fixtures/getFixtures/Fixtures200.json')
        * match response[*] == '#[3]'
        * match each response[*].fixtureId == '#present'

    Scenario: When requesting the /fixtures endpoint, each fixture should have basic details
        Given path '/fixtures'
        When method GET
        Then status 200
        * match response == read('../resources/fixtures/getFixtures/Fixtures200.json')
        * match each response[*].startDateTime == '#present'
        * match each response[*].footballFullState.homeTeam == '#string'
        * match each response[*].footballFullState.awayTeam == '#string'
        * match each response[*].footballFullState.finished == '#boolean'
        * match each response[*].footballFullState.gameTimeInSeconds == '#number'