# SBG - Bet Trading Tribe
## Technical Test Assessment

This test comes with a mock API server that represents a snapshot of our internal Trading Engine.

The challenge is to build an automation framework that explores and tests the endpoints provided.

Further information regarding each endpoint can be found in the file `apiDocs.html`, after you have downloaded the files.


### The Test

We realise that everyone has different levels of skills and experience when it comes to development,
and so if you do not have the time or knowledge to complete all tasks, that's okay,
we just want to see how you would approach the problem and get a feel for how you code.

Take no more than 1.5 to 3 hours on this test.

### Languages
This is a test automation focused test, so the end deliverable should be a test framework with various assertions
against the provided endpoints.

How you get there is up to you: we typically use Node.js or Kotlin, but you may use any mainstream language you like.

### Review Criteria
At a high level we will be looking for:

* Clear instructions for how to set up and run the framework on a reviewer's machine
* Good understanding of the tasks undertaken
* Well-structured and concise code
* Use of relevant design patterns
* Good understanding of errors and how to handle them

### Submission
Please upload your completed test to a publicly-accessible repository in a hosting service (e.g. GitHub),
and send a link to your recruitment contact.

## Installation
### Pre-requisites
* LTS version of NodeJS, and NPM: https://nodejs.org/

### Usage
Start the mock API server:
`npm install && npm run start`

It runs by default on `http://localhost:3000`

## Tasks

Using the provided API:

1. Retrieve all fixtures.
    1. Assert that there are 3 fixtures within the returned object.
    1. Assert that each of the 3 fixtures has a fixtureId value.
1. Using the model guide in `apiDocs.html`, store a new fixture in the database. To simulate latency within systems, there is an intentional, random delay to store a new fixture on the server.
    1. Bearing the delay in mind, retrieve it as soon as it's available and assert, within the `teams` array, that the first object has a `teamId` of 'HOME'.
1. Update a fixture within the database.
    1. Assert that the relevant attributes in the fixture have changed.
1. Delete a fixture.
    1. Assert that the fixture no longer exists.

## How to run functional tests?

This testing framework has been built using an API testing tool called Karate https://github.com/intuit/karate.

It is built and ran in the Java SDK with the package manager Maven.

### Pre-requesites
* You will need an up to date version of Java and Maven, If you do not have these you can install these here - https://www.oracle.com/java/technologies/javase-downloads.html https://maven.apache.org/install.html or use homebrew e.g. brew install maven
* Verify the installation of these by running: java --version & mvn --version
* Run the Usage section 


### Commands
* Ensure you're in the `tests/` directory

* Run the full suite:  `mvn test -Dtest=TestRunner`

## Future Improvements
* in the `getFixtureById.feature`, replace retry until responseStatus = 200 to be = JsonFileOfMockedFixture
* in the `getFixtureById.feature`, once the POST request can reliably give a 200 for a successfuly created fixture, then remove the `* def result` line and replace with Then status 200.
* implement a mocking tool such as wiremock to mock HTTP requests and responses
* setup a docker network e.g. docker-compose and dockerfile
* Integrate with CI/CD pipelines
* reduce logging level of the console output after a test run for readability and to make it easier to 
see test results.
* implement after hooks, to ensure fixture is deleted after the getFIxtureById.feature is ran
* in the `getFixtureById.feature`, grab `fixtureId` from the new stored fixture to replace using a hardcoded number, which covers the scenario where someone updates the `fixtureId` from `fixture.json` & `updatedFixture.json` causing the tests to then fail.
* create a custom type of validation for asserting against datetime values
 
## Alternative frameworks
* Use cucumber JS, with jsonPath, and a HTTP library.
* Setup karate in a JS project

